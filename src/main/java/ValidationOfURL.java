import java.io.IOException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.openqa.selenium.WebElement;

public class ValidationOfURL {

	

	public static void checkBrokenLinks(String link) throws IOException {
		
		
		URL url = new URL(link);
		
		
		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection() ;
		
		connection.setConnectTimeout(50000);
		
		connection.connect();
		
		
		if(connection.getResponseCode()==200) {
			
			
		}
		else {
			System.out.println( "Broken url  : "+ url  +"     with status code  : " + connection.getResponseCode());
		}
	}

	
}
