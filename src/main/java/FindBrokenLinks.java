
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindBrokenLinks {

	private static WebDriver driver;

	static Properties config = new Properties();


	@BeforeClass
	public static void setUp() {

		// Create ChromeDriver instance
		driver = new ChromeDriver();

		try (InputStream input = new FileInputStream( System.getProperty("user.dir") + "/config.properties")) {
			config.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void links() {

		driver.get(config.getProperty("url"));
		List<WebElement> list = driver.findElements(By.tagName("a"));

		list.forEach(links -> {

			String link = links.getAttribute("href");
			try {
				ValidationOfURL.checkBrokenLinks(link);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
	}

	@AfterClass
	public static void closing() {
		driver.quit();
	}

}
